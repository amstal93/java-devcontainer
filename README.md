# java{{version}}-devcontainer

This project gives a full development environment containing the open JDK version of your chouce and access to
docker. It makes use of the `vscode` feature of developing code entirely within a container env.
This makes it easier to develop apps in a self contained envirionment with all the dev dependencies 
needed in the docker container. For other developers working on the same project, they can simply 
grab this docker image from a registry and no other setup is required.

The `devcontainer.json` file is an example file that can be used to specify all the plugins that 
need to be installed when creating a stable, reproduceable development environment. Just change to
your username locally. This file should be checked into the repository so that it is sharable with
the team.

When building the image use your username as a build parameter

```bash
version=14
docker build . -t java$version-devcontainer --build-arg USERNAME=$(whoami) --build-arg VARIANT=$version 
```